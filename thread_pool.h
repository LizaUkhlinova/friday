#include <iostream>
#include <thread>
#include <mutex>
#include <atomic>
#include <vector>
#include <functional>
#include <future>
#include <exception>
#include <stdexcept>

static const int queue_default_capacity = 100;

template <class Value, class Container = std::deque <Value> >
class thread_safe_queue {
private:
	Container q;
	size_t queue_capacity;
	std::mutex blocking_mutex;
	std::condition_variable queue_is_not_empty;
	std::condition_variable queue_is_not_full;
	bool shutdown_flag;
public:
	explicit thread_safe_queue(size_t capacity = queue_default_capacity) : queue_capacity(capacity), shutdown_flag(true) {}
	void enqueue(Value item) {
		std::unique_lock<std::mutex> lock(blocking_mutex);
		if (shutdown_flag) {
			while (q.size() == queue_capacity)
				queue_is_not_full.wait(lock);
			q.push_back(std::move(item));
			queue_is_not_empty.notify_one();
		}
		else
			throw std::exception();
	}
	void pop(Value & item) {
		std::unique_lock<std::mutex> lock(blocking_mutex);
			while (q.empty()) {
				if (!shutdown_flag) {
					throw std::exception();
				}
				queue_is_not_empty.wait(lock);
			}
			item = std::move(q.front());
			q.pop_front();
			queue_is_not_full.notify_one();

	}
	void shutdown() {
		std::lock_guard<std::mutex> lock(blocking_mutex);
		shutdown_flag = false;
		queue_is_not_full.notify_all();
		queue_is_not_empty.notify_all();
	}
};

template <class ResultType>
struct task {
	std::function <ResultType()> task_function;
	std::promise <ResultType> task_promise;

	task(std::function <ResultType()> func, std::promise <ResultType> prms) : task_function(func), task_promise(std::move(prms)) {}
	task() {}
};

static const int worker_thread_default_number = 10;

template <class R>
class thread_pool {
private:
	const size_t worker_threads_number;
	thread_safe_queue <task <R> > Queue;
	std::vector <std::thread> worker_threads;
	std::atomic <bool> shutdown_flag;

	static size_t default_num_workers() {
		size_t def_num = std::thread::hardware_concurrency();
		if (!def_num) def_num = worker_thread_default_number;
		return def_num;
	}
public:
	explicit thread_pool(size_t threads_number = default_num_workers()) : worker_threads_number(threads_number), shutdown_flag(true) {
		for (size_t i = 0; i < threads_number; i++) {
			worker_threads.emplace_back([this]() {
				while (1) {
					try {
						task <R> new_task;
						Queue.pop(new_task);
						try {
							new_task.task_promise.set_value(new_task.task_function());
						}
						catch (...) {
							std::exception_ptr eptr = std::current_exception();
							new_task.task_promise.set_exception(eptr);
						}
					}
					catch (...) {
						return;
					}
				}
			});
		}
	}
	std::future<R> submit(std::function<R()> func) {
		if (!shutdown_flag.load()) {
			throw std::exception();
		}
		std::promise <R> prom;
		std::future <R> fut = prom.get_future();
		task <R> new_task(func, std::move(prom));
		Queue.enqueue(std::move(new_task));
		return std::move(fut);
	}
	void shutdown() {
		Queue.shutdown();
		shutdown_flag.store(false);
		for (size_t i = 0; i < worker_threads.size(); i++) {
			worker_threads[i].join();
		}
	}
};