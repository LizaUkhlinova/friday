#include <iostream>
#include <condition_variable>
#include <string>
#include <mutex>
#include <thread>

class semaphore {
private:
	std::mutex blocking_mutex;
	std::condition_variable signal_cv;
	size_t count;
public:
	semaphore() : count(0) {}
	void wait() {
		std::unique_lock<std::mutex> lock(blocking_mutex);
		while (count == 0) {
			signal_cv.wait(lock);
		}
		count -= 1;
	}
	void signal() {
		std::unique_lock<std::mutex> lock(blocking_mutex);
		count += 1;
		signal_cv.notify_one();
	}
};

class robot {
private:
	semaphore step_right;
	semaphore step_left;
public:
	robot() {
		step_left.signal();
	}
	void step(std::string side) {
		if (side == "left") {
			step_left.wait();
			std::cout << "step(\"left\")\n";
			step_right.signal();
		}
		else {
			step_right.wait();
			std::cout << "step(\"right\")\n";
			step_left.signal();
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(500));
	}
};


int main() {
	static const int ThreadNumber = 2, RunTime = 25;
	robot R;
	for (int i = 0; i < ThreadNumber; i++) {
		std::thread t([&R, i]() {
			std::string side = (i == 0) ? "left" : "right";
			int count = 0;
			while (count < RunTime) {
				R.step(side);
				count++;
			}
		});

		t.detach();
	}

	std::this_thread::sleep_for(std::chrono::milliseconds(5000));
	return 0;
}