#include <iostream>
#include <mutex>
#include <thread>

class barrier {
private:
	const size_t num_threads;
	std::mutex blocking_mutex;
	std::condition_variable all_threads_came;
	size_t free_places;
	size_t era;
public:
	explicit barrier(size_t num) : num_threads(num), free_places(num), era(0) {}
	void enter() {
		std::unique_lock<std::mutex> lock(blocking_mutex);
		size_t this_thread_era = era;
		free_places--;
		if (free_places == 0) {
			free_places = num_threads;
			era++;
			all_threads_came.notify_all();
		}
		else {
			while (era == this_thread_era) {
				all_threads_came.wait(lock);
			}
		}
	}
};