#include <iostream>
#include <thread>
#include <atomic>
#include <vector>
#include <stack>

class peterson_mutex {
private:
	std::atomic<bool> want[2];
	std::atomic<int> victim;
public:
	peterson_mutex() {
		want[0].store(false);
		want[1].store(false);
		victim.store(0);
	}
	peterson_mutex(const peterson_mutex & M) {
		want[0].store(M.want[0]);
		want[1].store(M.want[1]);
		victim.store(M.victim);
	}
	void lock(int t) {
		want[t].store(true);
		victim.store(t);
		while (want[1 - t].load() && victim.load() == t) {
			std::this_thread::yield();
		}
	}
	void unlock(int t) {
		want[t].store(false);
	}
};

class tree_mutex {
private:
	std::vector <peterson_mutex> mutex_heap;
	std::size_t size;
public:
	tree_mutex(std::size_t num_threads) {
		size = 1;
		while (size < num_threads) {
			size = size << 1;
		}
		size *= 2;
		mutex_heap.resize(size);
	}
	void lock(std::size_t thread_index) {
		size_t new_index = thread_index + size;
		size_t parent_index = new_index / 2;
		while (parent_index) {
			mutex_heap[parent_index].lock(new_index % 2);
			new_index /= 2;
			parent_index /= 2;
		}
	}
	void unlock(std::size_t thread_index) {
		std::stack <size_t> path;
		size_t new_index = thread_index + size;
		while (new_index > 1) {
			path.push(new_index);
			new_index /= 2;
		}
		while (!path.empty()) {
			new_index = path.top();
			path.pop();
			mutex_heap[new_index / 2].unlock(new_index % 2);
		}
	}
};
