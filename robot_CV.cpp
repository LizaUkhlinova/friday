#include <iostream>
#include <condition_variable>
#include <atomic>
#include <string>
#include <mutex>
#include <thread>

class robot {
private:
	bool last_side; 
	std::condition_variable last_step_was_left;
	std::condition_variable last_step_was_right;
	std::mutex blocking_mutex;
public:
	robot() : last_side(0) {}
	void step(std::string side) {
		std::unique_lock<std::mutex> lock(blocking_mutex);
		if (side == "left") {			
			if (last_side != 1) {
				last_step_was_right.wait(lock, [this]() {
					return (last_side == 1);
				});
			}
			std::cout << "step(\"left\")\n";
			last_side = 0;
			last_step_was_left.notify_one();
		}
		else {
			if (last_side != 0) {
				last_step_was_left.wait(lock, [this]() {
					return (last_side == 0);
				});
			}
			std::cout << "step(\"right\")\n";
			last_side = 1;
			last_step_was_right.notify_all();
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(500));
	}
};

int main() {
	static const int ThreadNumber = 2, RunTime = 25;
	robot R;
	for (int i = 0; i < ThreadNumber; i++) {
		std::thread t([&R, i]() {
			std::string side = (i == 0) ? "left" : "right";
			int count = 0;
			while (count < RunTime) {
				R.step(side);
				count++;
			}
		});

		t.detach();
	}

	std::this_thread::sleep_for(std::chrono::milliseconds(5000));
	return 0;
}